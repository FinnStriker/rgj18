Yleisimmät git-komennot<br>
Aloitus:<br>
Kopioi tiedostot tyhjään kansioon<br>
git clone [linkki gitlabistä]<br>
Aseta käyttäjänimi:<br>
git config --global user.name "[käyttäjänimi]"<br>
Aseta email:<br>
git config --global user.email "[email]"<br>

Päivittäinen (huom järjestys):<br>
Tarkista muutokset:<br>
git status<br>
Lisää kaikki tiedostot committiin:<br>
git add -A<br>
Lisää vain tietty tiedosto committiin:<br>
git add [tiedoston nimi]<br>
Luo commit:<br>
git commit -m"[viesti]"<br>
Hae muiden tekemät muutokset:<br>
git pull<br>
(huom. tässä välissä saattaa tulla erroreita, jolloin seuraava komento ei onnistu)<br>
Vie muutoksesi gitlabiin:<br>
git push<br>