﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public static PlayerPos playerPos1;
    public static PlayerPos playerPos2 = PlayerPos.light;
    public Sprite[] posSprites;
    public Image player1pos;
    public Image player2pos;

	

    protected void Update()
    {
        if (Input.GetButtonDown("SwitchSeat1"))
        {
            switch (playerPos1)
            {
                case PlayerPos.movement:
                    playerPos1 = PlayerPos.light;
                    player1pos.sprite = posSprites[0];
                    Debug.Log("P1 light");
                    break;
                case PlayerPos.light:
                    playerPos1 = PlayerPos.repair;
                    player1pos.sprite = posSprites[1];
                    Debug.Log("P1 repair");
                    break;
                case PlayerPos.repair:
                    playerPos1 = PlayerPos.waterpump;
                    player1pos.sprite = posSprites[2];
                    Debug.Log("P1 waterpump");
                    break;
                case PlayerPos.waterpump:
                    playerPos1 = PlayerPos.movement;
                    player1pos.sprite = posSprites[3];
                    Debug.Log("P1 movement");
                    break;
            }
        }
        if (Input.GetButtonDown("SwitchSeat2"))
        {
            switch (playerPos2)
            {
                case PlayerPos.movement:
                    playerPos2 = PlayerPos.light;
                    player2pos.sprite = posSprites[0];
                    Debug.Log("P2 light");
                    break;
                case PlayerPos.light:
                    playerPos2 = PlayerPos.repair;
                    player2pos.sprite = posSprites[1];
                    Debug.Log("P2 repair");
                    break;
                case PlayerPos.repair:
                    playerPos2 = PlayerPos.waterpump;
                    player2pos.sprite = posSprites[2];
                    Debug.Log("P2 waterpump");
                    break;
                case PlayerPos.waterpump:
                    playerPos2 = PlayerPos.movement;
                    player2pos.sprite = posSprites[3];
                    Debug.Log("P2 movement");
                    break;
            }
        }
    }
}

public enum PlayerPos
{
    movement,
    light,
    repair,
    waterpump
}
