﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(stress))]
public class SubHealth : MonoBehaviour {

    public int playerStartHealth = 100;
    public int playerCurrentHealth;
    public int amountOfRepair = 1;
    public int amountOfWaterToPump = 1;
    public int maxWaterLevel = 100;
    public int CurrentWaterLevel = 0;
    public float waterTic = 1.0f;
    private float countdown;
    private float bubbleCountdown;
    public float bubbleTic = 0.5f;
    public GameObject bubbles;
    public Sprite[] submarineSprites;

    stress stressi;

    public GameObject endScreen;

	void Start () {
        //health määrä alussa
        playerCurrentHealth = playerStartHealth;
        //veden määrä alussa
        CurrentWaterLevel = 0;
        countdown = waterTic;

        //alustetaan vesikuplat
        bubbleCountdown = bubbleTic;

        stressi = GetComponent<stress>();

    }

    // Jos health tippuu nollaan, tai alle.
    void Update()
    {
        if (playerCurrentHealth <= 0 || CurrentWaterLevel >= maxWaterLevel)
        {
            //TODO: kuolema / healt loppuuu
            GetComponent<SpriteRenderer>().sprite = submarineSprites[4];

            endScreen.SetActive(true);
            //Time.timeScale = 0.01f;
        }

        //lisätään vettä sekunnin välein joku määrä
        countdown -= Time.deltaTime;
        if (countdown <= 0.0f)
        {

            if (playerCurrentHealth < playerStartHealth * 0.8)
            {
                CurrentWaterLevel += 1;
                countdown = waterTic;
                bubbleTic = 1.0f;
                GetComponent<SpriteRenderer>().sprite = submarineSprites[0];
            }

            if (playerCurrentHealth < playerStartHealth * 0.8 && playerCurrentHealth > playerStartHealth * 0.6)
            {
                CurrentWaterLevel += 1;
                countdown = waterTic;
                bubbleTic = 0.5f;
                GetComponent<SpriteRenderer>().sprite = submarineSprites[1];
            }
            if (playerCurrentHealth < playerStartHealth * 0.6 && playerCurrentHealth > playerStartHealth * 0.4)
            {
                CurrentWaterLevel += 2;
                countdown = waterTic;
                bubbleTic = 0.4f;
                GetComponent<SpriteRenderer>().sprite = submarineSprites[2];
            }
            if (playerCurrentHealth < playerStartHealth * 0.4 && playerCurrentHealth > playerStartHealth * 0.2)
            {
                CurrentWaterLevel += 4;
                countdown = waterTic;
                bubbleTic = 0.3f;
                GetComponent<SpriteRenderer>().sprite = submarineSprites[2];
            }
            if (playerCurrentHealth < playerStartHealth * 0.2 && playerCurrentHealth > 0)
            {
                CurrentWaterLevel += 8;
                countdown = waterTic;
                bubbleTic = 0.2f;
                GetComponent<SpriteRenderer>().sprite = submarineSprites[3];
            }
        }

        bubbleCountdown -= Time.deltaTime;
        if (bubbleCountdown <= 0.0f)
        {
            spawnBubble(1);
            bubbleCountdown = bubbleTic;
        }

        // Player actions
        if (Player.playerPos1 == PlayerPos.repair)
        {
            if (Input.GetButtonDown("Action1"))
            {
                Debug.Log("repair");
                repairSub();
            }
        }
        else if (Player.playerPos1 == PlayerPos.waterpump)
        {
            if (Input.GetButtonDown("Action1"))
            {
                Debug.Log("pump");
                pumpWaterFromSub();
            }
        }
        if (Player.playerPos2 == PlayerPos.repair)
        {
            if (Input.GetButtonDown("Action2"))
            {
                Debug.Log("repair");
                repairSub();
            }
        }
        else if (Player.playerPos2 == PlayerPos.waterpump)
        {
            if (Input.GetButtonDown("Action2"))
            {
                Debug.Log("pump");
                pumpWaterFromSub();
            }
        }
    }

    //luodaan kupla
    void spawnBubble(int amount){
        for (int i = 0; i < amount; i++){
            Instantiate(bubbles, transform.position, Quaternion.identity);
        }
    }
    //Otetaan tietty määrä damagea
    public void takeDamage(int amountOfDamage){
        spawnBubble(3);
        playerCurrentHealth -= amountOfDamage;
        stressi.collisionHappened();

    }

    //korjataan ennalta määritelty määrä
    void repairSub()
    {
        if (playerCurrentHealth + amountOfRepair <= playerStartHealth)
        {
            playerCurrentHealth += amountOfRepair;
        }
        else if (playerCurrentHealth + amountOfRepair > playerStartHealth && playerCurrentHealth < playerStartHealth)
            playerCurrentHealth = playerStartHealth;
    }

    void pumpWaterFromSub(){
        if (CurrentWaterLevel - amountOfWaterToPump > 0)
        {
            CurrentWaterLevel -= amountOfWaterToPump;
        }
        else if (CurrentWaterLevel - amountOfWaterToPump <= 0 && CurrentWaterLevel > 0)
            CurrentWaterLevel = 0;
    }



}
