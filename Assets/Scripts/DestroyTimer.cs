﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Made by Niko Luukkonen for 'Griku' by Kuura Playhouse
// Destroys parent after set time

public class DestroyTimer : MonoBehaviour
{
    public float timer;

    void FixedUpdate()
    {
        if (timer <= 0)
        {
            Destroy(gameObject);
        }

        timer -= Time.deltaTime;
    }
}
