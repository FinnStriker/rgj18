﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseEnemy : MonoBehaviour {

    protected float OriginalMovementSpeed = 5;
    [SerializeField]
    protected float movementSpeed =5;
    public float MovementSpeed { get { return movementSpeed; } }
    protected bool knockedBack = false;
    [SerializeField]
    protected bool knockBackEnabled;
    public bool slowed;

    private void Start()
    {
        OriginalMovementSpeed = MovementSpeed;
    }
    public void ChangeMovementSpeed(float newSpeed, float duration)
    {
        if (newSpeed < OriginalMovementSpeed)
        {
            slowed = true;
        }
        movementSpeed = newSpeed;
        CancelInvoke("OriginalSpeed");
        Invoke("OriginalSpeed",duration);
    }

    protected void OriginalSpeed()
    {
        movementSpeed = OriginalMovementSpeed;
        slowed = false;
    }

   public virtual void KnockBack(Vector2 direction, float force, float time)
    {
        if (knockBackEnabled == false ||knockedBack)
            return;
        knockedBack = true;
        GetComponent<Rigidbody2D>().velocity = direction.normalized * force;
        Invoke("KnockBackEnded", time);
    }

    protected virtual void KnockBackEnded()
    {
        knockedBack = false;
    }
}
