﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrabAI : BaseEnemy
{
    // Players' submarine
    public Transform targetObject;

    //if this is set to true the AI will find a new waypoint
    bool waypointReached = false;

    //current target position
    Vector2 currentWaypoint;

    //are we currently moving
    bool moving = false;
    Vector2 prevMove;

    Rigidbody2D rb;

    //current direction of movement
    Vector2 moveDir;


    //how often to check distances between waypoints
    float distanceCheckInterval = 0.5f;
    //last distance check time
    float lastDistanceCheck = 0;
    float lastWaypointSetTime = 0;

    public enum MoveMode { Chase, Stay };
    public MoveMode mode;

    public float AttackDelay = 1f;
    public int Damage = 5;
    private bool m_attacking = false;
    public float playerStopDistance = 1.5f;    //enemy stops moving when player is closer than this

    public float maxPlayerChaseDistance = 8;//enemy returns to core state when player distance exceeds this
    public float aggroDistance = 4;   //radius to check for players when in core mode

    private float lastPlayerScan;    //last scan time for players
    private float playerScanInterval = 0.5f;  //how often the AI checks for players in radius
    int XfidgetDirection = 1;

    Animator anim;

    Vector2 getMoveDirection(Vector2 waypoint)
    {
        Vector2 dir = waypoint - new Vector2(transform.position.x, transform.position.y);
        dir.y = 0;
        dir.Normalize();

        return dir;
    }

    private void Start()
    {
        OriginalMovementSpeed = MovementSpeed;
        rb = GetComponent<Rigidbody2D>();
        SetTarget(ClosestPlayerInRadius(), 1);

        if (anim == null)
            anim = GetComponent<Animator>();
    }

    void FixedUpdate()
    {
        if (!targetObject || knockedBack)
            return;
        
        Debug.DrawLine(transform.position, currentWaypoint);

        if (mode == MoveMode.Chase) // default mode for AI
        {
            //if (moving)
            //{
            if (waypointReached == false) // if we have not reached the current waypoint we move towards it
            {
                moveDir = getMoveDirection(currentWaypoint); // set rigidbody velocity and direction
                moveDir *= MovementSpeed;
                moveDir.y = rb.velocity.y;
                rb.velocity = moveDir;
            }
            else if (waypointReached == true) // if we reached the last waypoint and we are grounded we set a new target
            {
                currentWaypoint = targetObject.position;
                lastWaypointSetTime = Time.time;
                waypointReached = false;
            }

            if (Time.time > lastDistanceCheck + distanceCheckInterval && waypointReached == false) //check if we reached waypoint
            {
                if (Vector2.Distance(transform.position, new Vector2(currentWaypoint.x, transform.position.y)) < 0.2f)
                {
                    lastDistanceCheck = Time.time;
                    waypointReached = true;
                    moveDir = Vector2.zero;
                    moveDir.y = rb.velocity.y;

                    if (transform.position.y < targetObject.position.y) // if we are on ground level we set our target to be the core
                    {
                        currentWaypoint = new Vector2(targetObject.position.x, targetObject.position.y);
                        lastWaypointSetTime = Time.time;
                    }
                }



            }

            //BUG PREVENTION
            //calculates a new waypoint every five seconds to make sure that AI doesn't get stuck fidgeting around
            if (Time.time > lastWaypointSetTime + 5)
            {
                lastWaypointSetTime = Time.time;
                currentWaypoint = targetObject.position;
                waypointReached = false;
            }
            //}


        }
        ////we are chasing a player and the player is not null
        //else 
        if (mode == MoveMode.Chase && targetObject != null)
        {
            moveDir = getMoveDirection(targetObject.position); //move towards player location
            moveDir *= MovementSpeed;
            moveDir.y = rb.velocity.y;


            if (targetObject.position.y > transform.position.y + 1.5f && Mathf.Abs(targetObject.position.x - transform.position.x) <= 2f)
            {
                moveDir.x = MovementSpeed * XfidgetDirection;

                if (targetObject.position.x >= transform.position.x + 1.5f && XfidgetDirection == -1)
                {
                    XfidgetDirection = 1;
                    Flip(true);
                }
                else if (targetObject.position.x <= transform.position.x - 1.5f && XfidgetDirection == 1)
                {
                    XfidgetDirection = -1;
                    Flip(false);
                }
            }

            rb.velocity = moveDir;
            //stop moving if we are close enough to the player
            if (Vector2.Distance(transform.position, targetObject.position) < playerStopDistance)
            {
                prevMove = moveDir;
                moveDir.x = 0;
                rb.velocity = new Vector2(0f, rb.velocity.y);
                moving = false;
                Attack(targetObject);
            }
            else if (Vector2.Distance(transform.position, targetObject.position) > playerStopDistance && !moving)
            {
                moveDir = prevMove;
                moving = true;
                currentWaypoint = targetObject.position;
                lastWaypointSetTime = Time.time;
                waypointReached = false;
            }

            //if the target player is too far or we don't see it we return to normal mode
            else if (Vector2.Distance(transform.position, targetObject.position) > maxPlayerChaseDistance || !lineOfSight(targetObject))
            {
                ChangeMode(MoveMode.Stay, null);
            }

        }
        if (mode == MoveMode.Chase && targetObject == null)
            ChangeMode(MoveMode.Stay, null);

        if (mode != MoveMode.Chase)
        {
            //scan for players and change mode to player chase if player is found within aggro radius and we have line of sight
            if (Time.time > lastPlayerScan + playerScanInterval)
            {
                lastPlayerScan = Time.time;
                Transform t = ClosestPlayerInRadius();
                if (t != null)
                    ChangeMode(MoveMode.Chase, t);
            }
        }
    }

    protected void Attack(Transform _target)
    {
        if (!m_attacking)
        {
            if (_target.GetComponent<SubHealth>() != null)
            {
                _target.GetComponent<SubHealth>().takeDamage(Damage);
                StartCoroutine(DelayAttack());
            }
        }
    }

    private IEnumerator DelayAttack()
    {
        m_attacking = true;
        anim.SetBool("Attacking", true);
        yield return new WaitForSeconds(AttackDelay);
        anim.SetBool("Attacking", false);
        m_attacking = false;
    }

    /// <summary>
    /// Finds closest player with line of sight if possible
    /// </summary>
    /// <returns></returns>
    Transform ClosestPlayerInRadius()
    {
        Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, aggroDistance, 1 << LayerMask.NameToLayer("Player"));
        float distance = Mathf.Infinity;
        Transform chosen = null;
        for (int i = 0; i < cols.Length; i++)
        {
            float d = Vector2.Distance(transform.position, cols[i].transform.position);
            if (d < distance && lineOfSight(cols[i].transform))
            {
                distance = d;
                chosen = cols[i].transform;
            }
        }

        return chosen;
    }


    /// <summary>
    /// Initializer function called by the spawner nest script
    /// </summary>
    /// <param name="t"></param>
    public void SetTarget(Transform t, float moveSpeedMultiplier)
    {
        targetObject = t;
        movementSpeed *= moveSpeedMultiplier;
        rb = GetComponent<Rigidbody2D>();
        currentWaypoint = targetObject.position;
        lastWaypointSetTime = Time.time;

        moving = true;

    }

    public void ChangeMode(MoveMode m, Transform t)
    {
        mode = m;
        if (mode == MoveMode.Chase)
        {
            targetObject = t;
        }
        else if (mode == MoveMode.Stay)
        {
            targetObject = null;
        }
    }

    //checks if there is no obstacles between the enemy and the target transform
    bool lineOfSight(Transform target)
    {
        if (target.gameObject.activeInHierarchy == false || target == null)
            return false;
        if (Physics2D.Linecast(transform.position + Vector3.up * 0.2f, target.position + Vector3.up * 0.2f, 1 << LayerMask.NameToLayer("Platforms")))
            return false;
        else return true;
    }
    void Flip(bool right)
    {
        if ((right && transform.localScale.x > 0) || (!right && transform.localScale.x < 0))
            transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }
}
