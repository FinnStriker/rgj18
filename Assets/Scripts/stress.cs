﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class stress : MonoBehaviour {


    public float currenStressLevel = 0f;
    public float maxStressLevel = 100f;
    private float countdown;
    public float stressReleaseTime = 0.05f;
    public int collisionDamage = 10;
    public GameObject stressLayer;
    private float stressLayerSize = 0.2f;
    Color alphaColor = Color.white;
    public Sprite[] player1Sprites;
    public Sprite[] player2Sprites;
    public Image player1;
    public Image player2;
    public AudioSource stressMusic;
    public AudioSource backgroundMusic;
    public AudioClip[] audioClips;

    // Use this for initialization
    void Start () {
        countdown = stressReleaseTime;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
      
            if (currenStressLevel >= 0)
            {
               currenStressLevel -= 0.1f;
            }
        stressLayerSize = 0.8f - (0.65f / 100 * currenStressLevel);
        stressLayer.transform.localScale = new Vector3(stressLayerSize, stressLayerSize, 1);
        alphaColor.a = 1f / 100f * currenStressLevel;
        stressLayer.GetComponent<SpriteRenderer>().color = alphaColor;
       
        //vaihdetaan naamat UI:hin
        if (currenStressLevel >= 75 && currenStressLevel <= 100){
            player1.sprite = player1Sprites[3];
            player2.sprite = player2Sprites[3];
            stressMusic.clip = audioClips[2];
            if (stressMusic.isPlaying == false){
                stressMusic.Play();
            }

        } else if (currenStressLevel >=50 && currenStressLevel <= 74){
            player1.sprite = player1Sprites[2];
            player2.sprite = player2Sprites[2];
            stressMusic.clip = audioClips[1];
            if (stressMusic.isPlaying == false)
            {
                stressMusic.Play();
            }
        } else if (currenStressLevel >=25 && currenStressLevel <= 49){
            player1.sprite = player1Sprites[1];
            player2.sprite = player2Sprites[1];
            stressMusic.clip = audioClips[0];
            if (stressMusic.isPlaying == false)
            {
                stressMusic.Play();
            }
        } else if (currenStressLevel <=0 && currenStressLevel <= 25){
            player1.sprite = player1Sprites[0];
            player2.sprite = player2Sprites[0];
            stressMusic.Pause();

        }

        if (stressMusic.isPlaying){
            backgroundMusic.volume = 0.5f;
        } else {
            backgroundMusic.volume = 1f;
        }


    }

    public void collisionHappened(){
        currenStressLevel += collisionDamage;
        if(currenStressLevel > 100){
            currenStressLevel = 100;
        }
    }

}
