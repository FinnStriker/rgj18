﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bubbles : MonoBehaviour {

    private float random;
    Color alphaColor = Color.white;
    public float lifeTime = 1.0f;
    private float countdown;
    public Sprite[] bubbleSprites;
    private int spriteRandom;

    // Use this for initialization
    void Start () {
        countdown = lifeTime/5;
        random = Random.Range(0.1f, 0.5f);
        spriteRandom = Random.Range(0, 3);
        GetComponent<SpriteRenderer>().sprite = bubbleSprites[spriteRandom];
        Destroy(gameObject, lifeTime);
        transform.localScale = new Vector3(random, random, 1.0f);
        random = Random.Range(-1f, 1f);
        transform.position += new Vector3(random, 0, 0);


    }
	
	// Update is called once per frame
	void Update () {
        transform.position += new Vector3(0, 0.1f, 0);

        countdown -= Time.deltaTime;
        if (countdown <= 0.0f)
        {
            //alpha värin vähennys
            alphaColor.a -= 0.2f;
            gameObject.GetComponent<SpriteRenderer>().color = alphaColor;
            countdown = lifeTime / 5;
        }
    }
}
