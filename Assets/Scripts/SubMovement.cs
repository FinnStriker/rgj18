﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// https://unity3d.com/learn/tutorials/projects/2d-ufo-tutorial/controlling-player

public class SubMovement : MonoBehaviour
{
    public float horizontalSpeed;               // Floating point variable to store the player's movement speed on horizontal axis
    public float verticalSpeed;                 // Floating point variable to store the player's movement speed on vertical axis

    private Rigidbody2D rb;       //Store a reference to the Rigidbody2D component required to use 2D Physics.

    // Use this for initialization
    void Start()
    {
        //Get and store a reference to the Rigidbody2D component so that we can access it.
        rb = GetComponent<Rigidbody2D>();
    }

    //FixedUpdate is called at a fixed interval and is independent of frame rate. Put physics code here.
    void FixedUpdate()
    {
        string hor = "", hor2 = "";
        string ver = "", ver2 = "";

        if (Player.playerPos1 == PlayerPos.movement)
        {
            hor = "Horizontal1";
            ver = "Vertical1";
        }
        if (Player.playerPos2 == PlayerPos.movement)
        {
            hor2 = "Horizontal2";
            ver2 = "Vertical2";
        }

        if (ver != "" && ver2 != "")
        {
            //Store the current horizontal input in the float moveHorizontal.
            float moveHorizontal = (Input.GetAxis(hor) + Input.GetAxis(hor2)) / 2;

            //Store the current vertical input in the float moveVertical.
            float moveVertical = (Input.GetAxis(ver) + Input.GetAxis(ver2)) / 2;

            //Use the two store floats to create a new Vector2 variable movement.
            Vector3 movement = new Vector3(moveHorizontal * horizontalSpeed, moveVertical * verticalSpeed, 0);

            //Call the AddForce function of our Rigidbody2D rb2d supplying movement multiplied by speed to move our player.
            rb.AddForce(movement);
        }
        else if (ver != "" && ver2 == "")
        {
            //Store the current horizontal input in the float moveHorizontal.
            float moveHorizontal = Input.GetAxis(hor);

            //Store the current vertical input in the float moveVertical.
            float moveVertical = Input.GetAxis(ver);

            //Use the two store floats to create a new Vector2 variable movement.
            Vector3 movement = new Vector3(moveHorizontal * horizontalSpeed, moveVertical * verticalSpeed, 0);

            //Call the AddForce function of our Rigidbody2D rb2d supplying movement multiplied by speed to move our player.
            rb.AddForce(movement);
        }
        else if (ver == "" && ver2 != "")
        {
            //Store the current horizontal input in the float moveHorizontal.
            float moveHorizontal = Input.GetAxis(hor2);

            //Store the current vertical input in the float moveVertical.
            float moveVertical = Input.GetAxis(ver2);

            //Use the two store floats to create a new Vector2 variable movement.
            Vector3 movement = new Vector3(moveHorizontal * horizontalSpeed, moveVertical * verticalSpeed, 0);

            //Call the AddForce function of our Rigidbody2D rb2d supplying movement multiplied by speed to move our player.
            rb.AddForce(movement);
        }

        
    }
}
