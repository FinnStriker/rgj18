﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubLight : MonoBehaviour
{
    public float smooth = 90.0f;
    public float tiltAngle = 90.0f;

    public GameObject lightObj;

    public bool LightPointingRight = true;
    public static bool LightOn = true;
    
    private void Update()
    {
        if (Player.playerPos1 == PlayerPos.light || Player.playerPos2 == PlayerPos.light)
        {
            // Rotate
            if (Input.GetButton("Vertical1") || Input.GetButton("Vertical2"))
            {
                string ver = "", ver2 = "";

                if (Player.playerPos1 == PlayerPos.light)
                {
                    ver = "Vertical1";
                }
                if (Player.playerPos2 == PlayerPos.light)
                {
                    ver2 = "Vertical2";
                }
                float tiltAroundZ = 0;
                if (ver != "" && ver2 != "")
                {
                    tiltAroundZ = (LightPointingRight) ? tiltAngle * ((Input.GetAxis(ver) + Input.GetAxis(ver2)) / 2) : -tiltAngle * ((Input.GetAxis(ver) + Input.GetAxis(ver2)) / 2);
                }
                else if (ver != "" && ver2 == "")
                {
                    tiltAroundZ = (LightPointingRight) ? tiltAngle * Input.GetAxis(ver) : -tiltAngle * Input.GetAxis(ver);
                }
                else if (ver == "" && ver2 != "")
                {
                    tiltAroundZ = (LightPointingRight) ? tiltAngle * Input.GetAxis(ver2) : -tiltAngle * Input.GetAxis(ver2);
                }

                Quaternion target = Quaternion.Euler(lightObj.transform.rotation.x, lightObj.transform.rotation.y, tiltAroundZ + 90);

                // Dampen towards the target rotation
                lightObj.transform.rotation = Quaternion.Slerp(lightObj.transform.rotation, target, Time.deltaTime * smooth);
            }
            // Flip
            if (Input.GetButton("Horizontal1") || Input.GetButton("Horizontal2"))
            {
                string hor = "", hor2 = "";

                if (Player.playerPos1 == PlayerPos.light)
                {
                    hor = "Horizontal1";
                }
                if (Player.playerPos2 == PlayerPos.light)
                {
                    hor2 = "Horizontal2";
                }

                float flip = 0;
                if (hor != "" && hor2 != "")
                {
                    flip = (Input.GetAxis(hor) + Input.GetAxis(hor2)) / 2;
                }
                else if (hor != "" && hor2 == "")
                {
                    flip = Input.GetAxis(hor);
                }
                else if (hor == "" && hor2 != "")
                {
                    flip = Input.GetAxis(hor2);
                }

                if (flip == 0)
                    return;
                if (flip > 0 && !LightPointingRight)
                {
                    lightObj.transform.localScale = new Vector3(lightObj.transform.localScale.x, -lightObj.transform.localScale.y, lightObj.transform.localScale.z);
                    LightPointingRight = !LightPointingRight;
                }
                else if (flip < 0 && LightPointingRight)
                {
                    lightObj.transform.localScale = new Vector3(lightObj.transform.localScale.x, -lightObj.transform.localScale.y, lightObj.transform.localScale.z);
                    LightPointingRight = !LightPointingRight;
                }
            }
            // Light toggle
            if (Input.GetButtonDown("Action1"))
            {
                if (Player.playerPos1 == PlayerPos.light)
                {
                    lightObj.SetActive((LightOn) ? false : true);
                    LightOn = !LightOn;
                }
            }
            // Light toggle
            if (Input.GetButtonDown("Action2"))
            {
                if (Player.playerPos2 == PlayerPos.light)
                {
                    lightObj.SetActive((LightOn) ? false : true);
                    LightOn = !LightOn;
                }
            }
        }
    }
}