﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class endScreen : MonoBehaviour
{
    public void BackToMainMenu()
    {
        SceneManager.LoadScene("KristonScene");
    }
    public void QuitGame()
    {
        Application.Quit();
    }
}
