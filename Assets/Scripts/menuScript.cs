﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class menuScript : MonoBehaviour
{
    void QuitGame() { Application.Quit(); }
 
    public VideoPlayer video;
    public void VideoStart()
    {
        float videoLength;
        videoLength=(float)video.clip.length;
        Invoke("LoadGame", videoLength +2.0f);

    }
   
     
     public void LoadGame()
    {
        SceneManager.LoadScene("GameScene");
    }
}
